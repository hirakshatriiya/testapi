from django.db import models

# Create your models here.


class SoftDeleteModel(models.Model):
    created_on = models.DateTimeField(auto_now_add=True)
    created_by = models.IntegerField(default=-1)
    modified_on = models.DateTimeField(auto_now=True)
    modified_by = models.IntegerField(default=-1)
    is_active = models.BooleanField(default=True)
    is_deleted = models.BooleanField(default=False)
    deleted_on = models.DateTimeField(null=True, default=None, blank=True)

    class Meta:
        abstract = True

class Otp(SoftDeleteModel):
    email = models.CharField(unique=True, null=True, blank=True, max_length=200)
    otp = models.PositiveIntegerField(null=True, blank=True)
    daily_sent_limit = models.PositiveIntegerField(default=0)
    verify_limit = models.PositiveIntegerField(default=0)
    used = models.BooleanField(default=False)
