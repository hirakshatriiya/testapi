
from django.urls import path

from Account.views import SendOTP, OtpVerify

urlpatterns = [
    path('send-otp', SendOTP.as_view(), name='send-otp'),
    path('otp_login', OtpVerify.as_view(), name='OtpVerify'),
]
