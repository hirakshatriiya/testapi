import random
from datetime import datetime, timedelta

import jwt
import pytz
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.core import mail

from django.http import JsonResponse
from django.shortcuts import render
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from rest_framework.views import APIView

from Account.models import Otp
from djangoProject import settings


# Create your views here.

class SendOTP(APIView):

    def post(self, request):
        otp = random.randint(1000, 9999)
        email = request.data.get('email')
        if not email:
            return JsonResponse({'message': 'email required'})
        otp_email = Otp.objects.filter(email=email).first()

        if otp_email:
            ist = pytz.timezone('Asia/Kolkata')
            current_time = datetime.now(ist)
            modified_on = otp_email.modified_on
            time1m = modified_on + timedelta(minutes=1)
            print(current_time)
            print(time1m)
            if current_time < time1m:
                return JsonResponse({'message': 'Please wait, you can resend otp after 1 min'})
            if otp_email.daily_sent_limit > 10 and otp_email.modified_on.date() <= datetime.now().date():
                return JsonResponse({'message': 'Limit exceed, you can resend after 24 hrs.'})
            elif otp_email.daily_sent_limit == 10:
                otp_email.daily_sent_limit = 1
            else:
                otp_email.daily_sent_limit = otp_email.daily_sent_limit + 1
            otp_email.otp = otp
            otp_email.used = False
        else:
            otp_email = Otp.objects.create(otp=otp, daily_sent_limit=1, email=email)

        otp_email.save()

        from_email = settings.EMAIL_HOST_USER
        html_message = render_to_string('otp_verify_email.html', context={'otp':otp})
        # send_mail('TestApi Verify your account', strip_tags(html_message), from_email, [email],html_message=html_message)
        subject = "TestApi Verify your account"
        mail.send_mail(subject, strip_tags(html_message), from_email,[email],html_message=html_message)


        return JsonResponse({'message':'Check email and Verify Email'})



class OtpVerify(APIView):
    def post(self, request):
        username = request.data.get('email')
        otp_code = request.data.get('otp')
        otp_email = Otp.objects.filter(email=username, otp=otp_code).first()
        if not otp_email:
            return JsonResponse({"message":"Invalid Otp"})
        ist = pytz.timezone('Asia/Kolkata')
        current_time = datetime.now(ist)
        modified_on = otp_email.modified_on
        time5m = modified_on + timedelta(minutes=5)
        time60m = modified_on + timedelta(minutes=60)



        if otp_email.used:
            return JsonResponse({"message":"OTP is already used"})
        if otp_email.verify_limit > 5:
            if current_time >= time60m:
                otp_email.verify_limit = 1
            else:
                return JsonResponse({"message":"Limit exceed, try again after 1 hour"})
        else:
            otp_email.verify_limit = otp_email.verify_limit + 1

        if current_time > time5m:
            return JsonResponse({"message":"OTP expired, please resend otp and try again"})
        otp_email.save()
        try:

            user = User.objects.filter(username=username).first()
            if not user:
                user = User.objects.create_user(username=username)
            token = generate_jwt_token(user)
            return JsonResponse({'token': token, "message":'Login successful'})

        except User.DoesNotExist:
            print('user not exist')

        return JsonResponse({'error': 'Invalid credentials'})



def generate_jwt_token(user):
    # Set the JWT secret key (make sure to keep this secret!)
    secret_key = 'asdcuniamj74fjnsg3jdnjdh3jdkhsjshskjs'

    # Define the payload for the JWT token
    payload = {
        'user_id': user.id,
        'username': user.username,
        'exp': datetime.utcnow() + timedelta(minutes=60)  # token will expire in 60 minutes
    }

    # Generate the JWT token
    token = jwt.encode(payload, secret_key, algorithm='HS256')

    # Return the token as a string
    return token


