# Generated by Django 4.1.7 on 2023-03-16 13:05

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Otp',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('created_by', models.IntegerField(default=-1)),
                ('modified_on', models.DateTimeField(auto_now=True)),
                ('modified_by', models.IntegerField(default=-1)),
                ('is_active', models.BooleanField(default=True)),
                ('is_deleted', models.BooleanField(default=False)),
                ('deleted_on', models.DateTimeField(blank=True, default=None, null=True)),
                ('email', models.CharField(blank=True, max_length=200, null=True, unique=True)),
                ('otp', models.PositiveIntegerField(blank=True, null=True)),
                ('daily_sent_limit', models.PositiveIntegerField(default=0)),
                ('verify_limit', models.PositiveIntegerField(default=0)),
                ('used', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
