from django.contrib import admin
from import_export.admin import ImportExportModelAdmin

from Account.models import Otp


# Register your models here.



class OtpAdmin(admin.ModelAdmin):
    list_per_page = 1000
    show_full_result_count = False
    list_display = [f.name for f in Otp._meta.fields]
    # resource_class = CategoryResource


admin.site.register(Otp, OtpAdmin)